﻿using CustomerOrderManagement.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerOrderManagement.Models
{
    public class CustomerOrderModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<Order> Orders { get; set; }

    }
}
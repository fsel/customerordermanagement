﻿using CustomerOrderAccess;
using CustomerOrderManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CustomerOrderManagement.Controllers
{
    public class OrderController : ApiController
    {
        //Add a new order for an existing customer
        public IHttpActionResult Post([FromBody]Order order)
        {
            using (var entity = new CustomerOrderEntities())
            {
                entity.Order.Add(new CustomerOrderAccess.Order()
                {
                    Price = order.Price,
                    CreateDate = order.CreateDate,
                    CustomerId = order.CustomerId
                });


                entity.SaveChanges();
            }

            return Ok();
        }

    }
}

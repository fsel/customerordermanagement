﻿using CustomerOrderAccess;
using CustomerOrderManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CustomerOrderManagement.Controllers
{
    public class CustomerController : ApiController
    {
        //Return the list of customers
        public IEnumerable<Customer> Get()
        {
            List<Customer> customer = new List<Customer>();
            foreach (var item in new CustomerOrderEntities().Customer.ToList())
            {
                try
                {
                    customer.Add(new Customer { Id = item.Id, Name = item.Name, Surname = item.Surname, Email = item.Email });
                }
                catch (Exception ex)
                {

                }
            }
            return customer;
        }

        //Return a customer and his orders
        public IEnumerable<CustomerOrderModel> Get(int CustomerId)
        {
            CustomerOrderModel customerOrderModel = new CustomerOrderModel();
            var customerData = new CustomerOrderEntities().Customer.ToList().FirstOrDefault(x => x.Id.Equals(CustomerId));
            Customer customer = new Customer()
            {
                Id = customerData.Id,
                Name = customerData.Name,
                Surname = customerData.Surname,
                Email = customerData.Email
            };

            List<Order> order = new List<Order>();

            foreach (var item in new CustomerOrderEntities().Order.ToList().Where(x => x.CustomerId.Equals(customer.Id)))
            {
                try
                {
                    order.Add(new Order { Id = item.Id, Price = item.Price, CreateDate = item.CreateDate ?? DateTime.Now, CustomerId = item.CustomerId ?? 1 });
                }
                catch (Exception ex)
                {

                }
            }

            customerOrderModel.Id = customer.Id;
            customerOrderModel.Name = customer.Name;
            customerOrderModel.Surname = customer.Surname;
            customerOrderModel.Orders = new List<Order>();
            customerOrderModel.Orders = order;

            yield return customerOrderModel;
        }

        //Add a new Customer
        public IHttpActionResult Post([FromBody]Customer customer)
        {
            using (var entity = new CustomerOrderEntities())
            {
                entity.Customer.Add(new CustomerOrderAccess.Customer()
                {
                    Name = customer.Name,
                    Surname = customer.Surname,
                    Email = customer.Email
                });

                entity.SaveChanges();
            }
            return Ok();
        }
    }
}

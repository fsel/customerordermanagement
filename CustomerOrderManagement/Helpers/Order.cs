﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomerOrderManagement.Controllers
{
    public class Order
    {
        public int Id { get; set; }
        public string Price { get; set; }
        public DateTime CreateDate { get; set; }
        public int CustomerId { get; set; }
    }
}